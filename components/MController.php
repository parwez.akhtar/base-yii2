<?php
namespace app\components;

use yii\web\Controller;
use app\models\User;

class MController extends Controller
{

    public function beforeAction($action)
    {
        if (! parent::beforeAction($action)) {
            return false;
        }
        $this->layout = 'guest-main';
        if (User::isAdmin()) {
            $this->layout = 'main';
        }
        return true;
    }

    public function renderNav()
    {
        $items = [
            self::addItem('Dashboard', 'dashboard/index', 'tachometer-alt', !User::isGuest()),
        ];
        return $items;
    }

    public static function addItem($label, $url, $icon, $visible)
    {
        $item = [
            'label' => $label,
            'url' => [
                $url
            ],
            'icon' => $icon,
            'visible' => $visible
        ];
        return $item;
    }
}