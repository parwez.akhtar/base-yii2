<?php
namespace app\components;

use diecoding\toastr\Toastr;
use diecoding\toastr\ToastrBase;

class FlashMessage extends ToastrBase
{

    public $closeButton = true;

    // public $debug = FALSE;

    // public $newestOnTop = TRUE;

    // public $progressBar = TRUE;
    public $positionClass = "toast-bottom-left";

    public $preventDuplicates = false;

    // public $onclick = NULL;
    public $showDuration = "300";

    public $hideDuration = "1000";

    public $timeOut = "7000";

    public $extendedTimeOut = "5000";

    public $showEasing = "swing";

    /**
     *
     * @var string $hideEasing swing | linear
     */
    public $hideEasing = "swing";

    /**
     *
     * @var string $showMethod show | fadeIn | slideDown
     */
    public $showMethod = "slideDown";

    /**
     *
     * @var string $hideMethod hide | fadeOut | slideUp
     */
    public $hideMethod = "slideUp";

    public $tapToDismiss = false;

    /**
     *
     * @inheritdoc
     */
    public function run()
    {
        $session = \Yii::$app->session;
        $flashes = $session->getAllFlashes();
        foreach ($flashes as $type => $data) {
            $datas = (array) $data;
            foreach ($datas as $key => $value) {
                Toastr::widget([
                    "type" => in_array($type, self::ALERT_TYPES) ? $type : "info",
                    "title" => is_array($datas[0]) ? ucfirst($value[0]) : ucfirst($type),
                    "message" => is_array($value[1]) ? $value[1] : $value,
                    "options" => $this->options
                ]);
            }
            $session->removeFlash($type);
        }
    }
}