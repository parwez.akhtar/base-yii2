<?php
use yii\helpers\Html;
use yii\bootstrap5\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap5\ActiveForm */
// $this->title = 'Signup';
$userModel = User::find()->where([
    'role_id' => User::ROLE_ADMIN
])->one();
if (empty($userModel)){
    $title = Yii::t('app', 'Add Admin');
}else {
    $title = Yii::t('app', 'Signup');
}
?>
<section class="login-signup py-5">
	<div class="container-fluid">
		<div class="row p-3 justify-content-center">
			<div class="order-1 col-lg-5 order-lg-2">
				<div class="login-box">
					<h3 id="profile-name" class="section-title"><?= $title?></h3>
					<br>
                    	<?php
                    $form = ActiveForm::begin([
                        'id' => 'form-signup',
                        'enableAjaxValidation' => false,
                        'options' => [
                            'class' => 'form-signin'
                        ]
                    ]);
                    ?>
                        <span id="reauth-email" class="reauth-email"></span>
                        
                        <?=$form->field($model, 'first_name', ['inputOptions' => ['class' => 'form-control form-control-user','placeholder' => 'Enter Your First Name']])->textInput()->label(false)?>
        				
        				<?=$form->field($model, 'last_name', ['inputOptions' => ['class' => 'form-control form-control-user','placeholder' => 'Enter Your Last Name']])->textInput()->label(false)?>
        				
        				<?=$form->field($model, 'email', ['inputOptions' => ['class' => 'form-control form-control-user','placeholder' => 'Enter Your Email']])->textInput()->label(false)?>
        				
        				
        				<?=$form->field ($model, 'password', [ 'template' => '{input}{error}'])->passwordInput(['maxlength' => true,'placeholder' => 'Password' ])->label(false)?>
        				
                        <?=$form->field ( $model, 'confirm_password', [ 'template' => '{input}{error}' ] )->passwordInput ( [ 'maxlength' => true,'placeholder' => 'Confirm Password' ] )->label ( false )?>
        				
        			    <?= Html::submitButton('Register Account', ['class' => 'btn btn-primary btn-user btn-block', 'name' => 'signup-button']) ?>
					
        				<!-- <hr>
        				<a href="index.html" class="btn btn-google btn-user btn-block"> <i
        					class="fab fa-google fa-fw"></i> Register with Google
        				</a> <a href="index.html"
        					class="btn btn-facebook btn-user btn-block"> <i
        					class="fab fa-facebook-f fa-fw"></i> Register with Facebook
        				</a> -->
						<?php ActiveForm::end()?>
				</div>
			</div>
		</div>
	</div>
</section>
