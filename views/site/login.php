<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap5\ActiveForm */
/* @var $model \app\models\LoginForm */
use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<section class="login-signup py-5">
	<div class="container-fluid">
		<div class=" row p-3 justify-content-center">
			<div class="order-1 col-lg-5 order-lg-2">
				<div class="login-box">
					<h3 id="profile-name" class="section-title">Log In</h3>

					<div class="">
						<span class="text-center"><br></span>
					</div>
            <?php
            $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => [
                    'class' => 'user'
                ]
            ])?>

                <span id="reauth-email" class="reauth-email"></span>
                
                
                     <?= $form->field ( $model, 'username' )->label ( false )->textInput ( [ 'placeholder' => $model->getAttributeLabel ( 'email' ) ] )?>
           			 <?= $form->field ( $model, 'password' )->label ( false )->passwordInput ( [ 'placeholder' => $model->getAttributeLabel ( 'password' ) ] )?>
           			
           			<div class="row">
						<div class="col-md-6">
							<div id="remember" class="checkbox">
                  				<?php //echo $form->field($model, 'rememberMe')->checkbox();?>
           					 </div>
						</div>

<!-- 						<div class="col-md-6"> -->
<!-- 							<a class="forgot-password float-none float-md-right" href="#0">Forgot Password? </a> -->
<!-- 						</div> -->
					</div>
                <?=Html::submitButton ( 'Login', [ 'class' => 'btn btn-info btn-block btn-signin mt-4 mt-md-0','id' => 'login','name' => 'login-button' ] )?>
                            <h4 class="text-center dont-text"></h4>
    
            <?php ActiveForm::end()?>
        </div>
			</div>
		</div>
		<!-- /card-container -->
	</div>
	<!-- /container -->
</section>
