<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use yii\bootstrap5\ActiveForm;
use yii\base\Controller;

class SiteController extends Controller
{

    public $layout = 'guest-main';

    /**
     *
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'logout'
                ],
                'rules' => [
                    [
                        'actions' => [
                            'signup'
                        ],
                        'allow' => true,
                        'roles' => [
                            '?'
                        ]
                    ],
                    [
                        'actions' => [
                            'login',
                            'error',
                            'forgot-password'
                        ],
                        'allow' => true
                    ],
                    [
                        'actions' => [
                            'logout'
                        ],
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    public function beforeAction($action)
    {
        if (! parent::beforeAction($action)) {
            return false;
        }
        if (User::isAdmin()) {
            $this->layout = 'main';
        }
        return true;
    }

    /**
     *
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (User::isAdmin()) {
            // $this->layout = 'main';
            return $this->redirect('dashboard/index');
        } else {
            $this->layout = 'guest-main';
            return $this->render('index');
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (! \Yii::$app->user->isGuest) {
            if (User::isUser()) {
                return $this->render(
                    '/site/index'
                );
            }
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            // TODO: change redirect to return url
            if (! User::isAdmin()) {
                \Yii::$app->getSession()->setFlash('success', 'Logged in successfully!');
                return $this->render(
                    '/site/index'
                );
            } else {
                return $this->render('index');
            }
        }
        return $this->render('login', [
            'model' => $model
        ]);
    }

    public function actionSignup()
    {
        $model = new User([
            'scenario' => 'signup'
        ]);
        $post = \Yii::$app->request->post();
        if (Yii::$app->request->isAjax && $model->load($post)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load($post)) {
            $userModel = User::find()->where([
                'role_id' => User::ROLE_ADMIN
            ])->one();
            if (empty($userModel)) {
                $model->role_id = User::ROLE_ADMIN;
            } else {
                $model->role_id = User::ROLE_USER;
            }
            $model->state_id = User::STATE_ACTIVE;
            $model->email_verified = User::EMAIL_VERIFIED;
            if ($model->validate()) {
                $model->scenario = 'add';
                $model->setPassword($model->password);
                $model->generatePasswordResetToken();
                $model->full_name = $model->first_name . " " . $model->last_name;
                if ($model->save()) {
                    \Yii::$app->getSession()->setFlash('success', "Signup Successfull.");
                    return $this->render(
                        'index'
                    );
                } else {
                    \Yii::$app->getSession()->setFlash('error', "Error !!" . $model->getErrorsString());
                }
            } else {
                \Yii::$app->getSession()->setFlash('error', "Error !!" . $model->getErrorsString());
            }
        }
        return $this->render('signup', [
            'model' => $model
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->render('index');
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
