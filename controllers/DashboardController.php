<?php
namespace app\controllers;

use app\models\User;
use app\components\MController;

class DashboardController extends MController
{

    public $layout = 'guest-main';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'ruleConfig' => [
                    'class' => \yii\filters\AccessRule::class
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index'
                        ],
                        'allow' => true,
                        'roles' => [
                            // '*',
                            // '?',
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    public function beforeAction($action)
    {
        if (! parent::beforeAction($action)) {
            return false;
        }
        $this->layout = 'guest-main';
        if (User::isAdmin()) {
            $this->layout = 'main';
        }
        return true;
    }

    public function actionIndex()
    {
        if (User::isAdmin()) {
            return $this->render('index');
        } else {
            return $this->redirect([
                'site/index'
            ]);
        }
    }
}
