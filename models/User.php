<?php
namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%tbl_user}}".
 *
 * @property int $id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $full_name
 * @property int|null $designation
 * @property string $email
 * @property string $password
 * @property string|null $date_of_birth
 * @property int|null $gender
 * @property string|null $about_me
 * @property string|null $contact_no
 * @property string|null $address
 * @property string|null $latitude
 * @property string|null $longitude
 * @property string|null $city
 * @property string|null $country
 * @property string|null $zipcode
 * @property string|null $language
 * @property string|null $profile_file
 * @property int|null $tos
 * @property int $role_id
 * @property int $state_id
 * @property int|null $type_id
 * @property string|null $last_visit_time
 * @property string|null $last_action_time
 * @property string|null $last_password_change
 * @property int|null $login_error_count
 * @property string|null $activation_key
 * @property string|null $access_token
 * @property string|null $timezone
 * @property string $created_on
 * @property string|null $updated_on
 * @property int|null $created_by_id
 * @property int $email_verified
 * @property int $push_enabled
 * @property int $email_enabled
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    
    public $confirm_password;
    
    public $newPassword;
    
    public $oldPassword;
    
    const ROLE_ADMIN = 0;
    
    const ROLE_MANAGER = 2;
    
    const ROLE_USER = 3;
    
    const EMAIL_NOT_VERIFIED = 0;
    
    const EMAIL_VERIFIED = 1;
    
    const STATE_INACTIVE = 0;
    
    const STATE_ACTIVE = 1;
    
    const STATE_BANNED = 2;
    
    const STATE_DELETED = 4;
    
    
    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "Inactive",
            self::STATE_ACTIVE => "Active",
            self::STATE_BANNED => "Banned",
            self::STATE_DELETED => "Deleted"
        ];
    }
    
    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }
    
    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "info",
            self::STATE_ACTIVE => "success",
            self::STATE_BANNED => "warning",
            self::STATE_DELETED => "danger"
        ];
        
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }
    
    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->created_on))
                $this->created_on = date('Y-m-d H:i:s');
                if (! isset($this->updated_on))
                    $this->updated_on = date('Y-m-d H:i:s');
                    if (! isset($this->created_by_id))
                        $this->created_by_id = \Yii::$app->user->id;
        } else {
            $this->updated_on = date('Y-m-d H:i:s');
        }
        
        return parent::beforeValidate();
    }
    
    public function getErrorsString()
    {
        $out = '';
        if ($this->errors != null)
            foreach ($this->errors as $err) {
                $out .= implode(',', $err);
            }
        return $out;
    }
    
    public static function getGenderOptions($id = null)
    {
        $list = array(
            self::MALE => "Male",
            self::FEMALE => "Female"
        );
        if ($id === null)
            return $list;
            return isset($list[$id]) ? $list[$id] : 'Not Defined';
    }
    
    public static function getRoleOptions($id = null)
    {
        $list = array(
            self::ROLE_ADMIN => "Admin",
            self::ROLE_MANAGER => "Manager",
            self::ROLE_USER => "User"
        );
        if ($id === null)
            return $list;
            return isset($list[$id]) ? $list[$id] : 'Not Defined';
    }
    
    public function getRole()
    {
        $list = self::getRoleOptions();
        return isset($list[$this->role_id]) ? $list[$this->role_id] : 'Not Defined';
    }
    
    /**
     *
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'full_name' => 'Full Name',
            'designation' => 'Designation',
            'email' => 'Email',
            'password' => 'Password',
            'date_of_birth' => 'Date Of Birth',
            'gender' => 'Gender',
            'about_me' => 'About Me',
            'contact_no' => 'Contact No',
            'address' => 'Address',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'city' => 'City',
            'country' => 'Country',
            'zipcode' => 'Zipcode',
            'language' => 'Language',
            'profile_file' => 'Profile File',
            'tos' => 'Tos',
            'role_id' => 'Role',
            'state_id' => 'Status',
            'type_id' => 'Type',
            'last_visit_time' => 'Last Visit Time',
            'last_action_time' => 'Last Action Time',
            'last_password_change' => 'Last Password Change',
            'login_error_count' => 'Login Error Count',
            'activation_key' => 'Activation Key',
            'access_token' => 'Access Token',
            'timezone' => 'Timezone',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'created_by_id' => 'Created By',
            'email_verified' => 'Email Verified',
            'push_enabled' => 'Push Enabled',
            'email_enabled' => 'Email Enabled'
        ];
    }
    
    /**
     *
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_user';
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['signup'] = [
            'first_name',
            'last_name',
            'full_name',
            'contact_no',
            'email',
            'password',
            'confirm_password'
        ];
        
        $scenarios['add'] = [
            'full_name',
            'first_name',
            'last_name',
            'zipcode',
            'email',
            'password',
            'confirm_password',
            'role_id',
            'state_id'
        ];
        $scenarios['update'] = [
            'full_name',
            'first_name',
            'last_name',
            'zipcode',
            'email',
            'address',
            'contact_no',
            'role_id'
        ];
        
        $scenarios['create'] = [
            'full_name',
            'first_name',
            'last_name',
            'zipcode',
            'email',
            'address',
            'contact_no',
            'role_id',
            'state_id'
        ];
        $scenarios['change-password'] = [
            'password',
            'confirm_password'
        ];
        return $scenarios;
    }
    
    /**
     *
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'designation',
                    'gender',
                    'tos',
                    'role_id',
                    'state_id',
                    'type_id',
                    'login_error_count',
                    'created_by_id',
                    'email_verified',
                    'push_enabled',
                    'email_enabled'
                ],
                'integer'
            ],
            [
                [
                    'first_name',
                    'last_name',
                    'email',
                    'password',
                    'confirm_password',
                    'role_id',
                    'state_id',
                    'created_on'
                ],
                'required',
                'on' => 'signup'
            ],
            
            [
                [
                    'first_name',
                    'last_name',
                    'email',
                    'address',
                    'contact_no',
                    'role_id',
                    'state_id',
                    'created_on'
                ],
                'required',
                'on' => 'create'
            ],
            [
                [
                    'password',
                    'confirm_password'
                ],
                'required',
                'on' => 'change-password'
            ],
            
            [
                [
                    'first_name',
                    'last_name',
                    'email',
                    'address',
                    'role_id',
                    'state_id',
                    'contact_no'
                ],
                'required',
                'on' => 'update'
            ],
            [
                'email',
                'unique'
            ],
            [
                'email',
                'email'
            ],
            [
                [
                    'date_of_birth',
                    'last_visit_time',
                    'last_action_time',
                    'last_password_change',
                    'created_on',
                    'updated_on'
                ],
                'safe'
            ],
            [
                [
                    'about_me'
                ],
                'string'
            ],
            [
                [
                    'first_name',
                    'last_name',
                    'full_name',
                    'password',
                    'activation_key',
                    'access_token'
                ],
                'string',
                'max' => 128
            ],
            [
                [
                    'profile_file'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'contact_no',
                    'latitude',
                    'longitude',
                    'country',
                    'language'
                ],
                'string',
                'max' => 32
            ],
            
            [
                'confirm_password',
                'compare',
                'compareAttribute' => 'password',
                'message' => "Passwords don't match",
                'on' => [
                    'signup',
                    'change-password'
                ]
            ],
            
            [
                'password',
                'app\components\validators\PasswordValidator'
            ],
            
            [
                [
                    'full_name'
                ],
                'app\components\validators\NameValidator'
            ],
            
            [
                [
                    'address'
                ],
                'string',
                'max' => 512
            ],
            [
                [
                    'city',
                    'timezone'
                ],
                'string',
                'max' => 64
            ],
            [
                [
                    'zipcode'
                ],
                'string',
                'max' => 16
            ]
        ];
    }
    
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ])->cache();
    }
    
    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        return $relations;
    }
    
    public function beforeDelete()
    {
        if (! parent::beforeDelete()) {
            return false;
        }
        if ($this->id == \Yii::$app->user->id)
            return false;
            
            if (self::find()->count() <= 1)
                return false;
                
                // Delete actual file
                $filePath = Yii::$app->basePath . '/uploads/' . $this->profile_file;
                
                if (is_file($filePath)) {
                    unlink($filePath);
                }
                
                return true;
    }
    
    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'state_id' => self::STATE_ACTIVE
        ]);
    }
    
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne([
            'activation_key' => $token,
            'state_id' => self::STATE_ACTIVE
        ]);
    }
    
    public static function findByEmail($email)
    {
        return static::findOne([
            'email' => $email,
            'state_id' => self::STATE_ACTIVE
        ]);
    }
    
    public function getUsername()
    {
        return substr($this->email, 0, strpos($this->email, '@'));
    }
    
    public static function findByUsername($username)
    {
        if (! strstr($username, '@')) {
            $username = $username . '@example.com';
        }
        return static::findByEmail($username);
    }
    
    public static function findByPasswordResetToken($token)
    {
        if (! static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'activation_key' => $token,
            'state_id' => self::STATE_ACTIVE
        ]);
    }
    
    public function getResetUrl()
    {
        return Yii::$app->urlManager->createAbsoluteUrl([
            'user/resetpassword',
            'token' => $this->activation_key
        ]);
    }
    
    public function getVerified()
    {
        return Yii::$app->urlManager->createAbsoluteUrl([
            'user/confirm-email',
            'id' => $this->activation_key
        ]);
    }
    
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }
    
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    
    public function getAuthKey()
    {
        return $this->activation_key;
    }
    
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    
    /* For signup */
    public function setPassword($password)
    {
        $this->password = $this->hashPassword($password);
    }
    
    public function hashPassword($password)
    {
        $password = utf8_encode(Yii::$app->security->generatePasswordHash(yii::$app->name . $password));
        return $password;
    }
    /* End of signup */
    
    /* Password validation */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword(yii::$app->name . $password, utf8_decode($this->password));
    }
    /* End of password validation */
    
    public function generateAuthKey()
    {
        $this->activation_key = Yii::$app->security->generateRandomString();
    }
    
    public function generatePasswordResetToken()
    {
        $this->activation_key = Yii::$app->security->generateRandomString() . '_' . time();
    }
    
    public function removePasswordResetToken()
    {
        $this->activation_key = null;
    }
    
    public static function isUser()
    {
        $user = Yii::$app->user->identity;
        if ($user == null)
            return false;
            
            return ($user->isActive() && $user->role_id == User::ROLE_USER || self::isManager());
    }
    
    public static function isManager()
    {
        $user = Yii::$app->user->identity;
        if ($user == null)
            return false;
            
            return ($user->isActive() && $user->role_id == User::ROLE_MANAGER || self::isAdmin());
    }
    
    public function isSelf()
    {
        if ($this->id == Yii::$app->user->identity->id)
            return true;
            
            return false;
    }
    
    public static function isAdmin()
    {
        $user = Yii::$app->user->identity;
        if ($user == null)
            return false;
            
            return ($user->isActive() && $user->role_id == User::ROLE_ADMIN);
    }
    
    public static function isGuest()
    {
        if (Yii::$app->user->isGuest) {
            return true;
        }
        return false;
    }
    
    public function isActive()
    {
        return ($this->state_id == User::STATE_ACTIVE);
    }
    
    public function getLoginUrl()
    {
        return Yii::$app->urlManager->createAbsoluteUrl([
            'user/login'
        ]);
    }
    
    public function isPermission()
    {
        if (User::isAdmin()) {
            return true;
        }
        
        return parent::isPermission();
    }
    
    public function saveUploadedFile($model, $attribute = 'image_file', $old = null)
    {
        $uploaded_file = UploadedFile::getInstance($model, $attribute);
        if ($uploaded_file != null) {
            $path = Yii::$app->basePath . '/uploads/';
            $filename = $path . str_replace('/', '-', Yii::$app->controller->id) . '-' . time() . '-' . $attribute . '-user_id_' . Yii::$app->user->id . '.' . $uploaded_file->extension;
            if (is_file($filename))
                unlink($filename);
                if (! empty($old) && is_file($path . $old))
                    unlink($path . $old);
                    $uploaded_file->saveAs($filename);
                    $model->$attribute = basename($filename);
                    return true;
        }
        return false;
    }
    
    public function getImageUrl()
    {
        return \Yii::$app->request->BaseUrl . '/uploads/' . $this->profile_file;
    }
}
