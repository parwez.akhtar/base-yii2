<?php
namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class UserSearch extends User
{

    /**
     *
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'designation',
                    'tos',
                    'type_id',
                    'login_error_count',
                    'created_by_id',
                    'email_verified',
                    'push_enabled',
                    'email_enabled'
                ],
                'integer'
            ],
            [
                [
                    'first_name',
                    'last_name',
                    'full_name',
                    'email',
                    'password',
                    'date_of_birth',
                    'about_me',
                    'contact_no',
                    'address',
                    'latitude',
                    'longitude',
                    'city',
                    'country',
                    'zipcode',
                    'language',
                    'profile_file',
                    'last_visit_time',
                    'last_action_time',
                    'last_password_change',
                    'activation_key',
                    'access_token',
                    'timezone',
                    'created_on',
                    'updated_on',
                    'role_id',
                    'gender',
                    'state_id'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        
        $this->load($params);
        
        if (! $this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'like',
            'first_name',
            $this->first_name
        ])
        ->andFilterWhere([
            'like',
            'email',
            $this->email
        ])
        ->andFilterWhere([
            'like',
            'gender',
            $this->gender
        ])
        ->andFilterWhere([
            'like',
            'contact_no',
            $this->contact_no
        ])
        ->andFilterWhere([
            'like',
            'address',
            $this->address
        ])
        ->andFilterWhere([
            'like',
            'role_id',
            $this->role_id
        ])
        ->andFilterWhere([
            'like',
            'state_id',
            $this->state_id
        ]);
        
        return $dataProvider;
    }

    public static function monthly($state = null, $created_by_id = null, $dateAttribute = 'created_on')
    {
        $date = new \DateTime(date('Y-m'));

        $date->modify('-1 year');

        $count = [];
        $query = self::find()->cache();
        for ($i = 1; $i <= 12; $i ++) {
            $date->modify('+1 months');
            $month = $date->format('Y-m');

            $query->where([
                'like',
                $dateAttribute,
                $month
            ]);

            if ($created_by_id !== null) {
                $query->andWhere([
                    'created_by_id' => $created_by_id
                ]);
            }

            if ($state !== null) {
                $state = is_array($state) ? $state : [
                    $state
                ];
                $query->andWhere([
                    'in',
                    'state_id',
                    $state
                ]);
            }

            $count[$month] = (int) $query->count();
        }
        return $count;
    }
}